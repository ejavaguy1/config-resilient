package info.ejava.examples.resilient.config_server;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.stream.IntStream;

import static org.assertj.core.api.BDDAssertions.then;

@SpringBootTest(
        properties = {"CONFIG_REPO=https://gitlab.com/ejavaguy1/config-resilient.git"},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@Slf4j
public class FailureNTest {
    private RestTemplate restTemplate = new RestTemplate();
    URI baseUrl;
    @Value("${spring.cloud.config.server.git.uri}")
    String gitUrl;

    @BeforeEach
    void init(@LocalServerPort int port) {
        baseUrl = UriComponentsBuilder.fromUriString("http://localhost:" + port).build().toUri();
    }

    @Test
    void testLookup() {
        //given
        URI url = UriComponentsBuilder.fromUri(baseUrl).path("/myapp/foo").build().toUri();
        RequestEntity request = RequestEntity.get(url).accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<String> response = restTemplate.exchange(request, String.class);
        //when
        IntStream.range(0,3).forEach(i-> restTemplate.exchange(request, String.class) );
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("{}", request.getBody());
    }
}
